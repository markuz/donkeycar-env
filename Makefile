.PHONY: help mqtt up upi

help:
	@echo "help  - this help"
	@echo "clean - remove all tub data"
	@echo "mqtt  - connect to LCA MQTT channel"
	@echo "up    - pip install -U -r requirements/base.txt"
	@echo "upi   - pip install -U -r requirements/pi.txt"

mqtt:
	mosquitto_sub -h test.mosquitto.org -t 'ohmc/#' -v

up:
	pip install -U -r requirements/base.txt

upi:
	pip install -U -r requirements/pi.txt
