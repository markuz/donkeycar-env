README
======

Overview
--------

This reository aims to help with `donkeycar <http://donkeycar.com>`_ management.

Goals:

* auto-start donkeycar's drive mode
* announce car's network info for easy discovery
* easy download of tubs data
* 

Python + virtualenv
-------------------

:: 

    sysctl -w net.ipv6.conf.all.disable_ipv6=1 # Reconnect via ssh afterwards
    sysctl -w net.ipv6.conf.default.disable_ipv6=1
::

    apt-get install -y i2c-tools
    i2cdetect -y 1
            0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
       00:          -- -- -- -- -- -- -- -- -- -- -- -- --
       10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
       20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
       30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
       40: 40 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
       50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
       60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
       70: 70 -- -- -- -- -- -- --


Finding your car on the network
-------------------------------

::

    apt-get install mosquitto-clients

    mosquitto_sub -h test.mosquitto.org -t 'ohmc/#' -v
     ohmc/ohmc_01 10.193.2.69 Mon 11 Jun 07:08:01 UTC 2018


Driving the car manually
------------------------

::

    python manage.py drive

Data acquisition for the Neural Network
---------------------------------------

::

    rsync -av pi@<car_ip>:ohmc_car .

When finished acquisition, then transfer the data from the DonkeyCar (Raspberry Pi) to your laptop / desktop for training the Neural Network::

    rsync -av pi@<car_ip>:ohmc_car/tub ohmc_car/tub_$DATE

Training the Neural Network
---------------------------

Once training data has been copied to your laptop / desktop, you can begin training the Neural Network::

    python manage.py train --tub $HOME/ohmc_car/tub_$DATE --model ./models/model_$DATE.hdf5

When training has completed, copy the trained model back to your DonkeyCar (Raspberry Pi)::

    scp $USERNAME@$HOSTNAME:ohmc_car/models/model_$DATE.hdf5 models

Self-driving
------------

::

    python manage.py drive --model ~/ohmc_car/models/models/model_$DATE.hdf5

This works similar to the manual driving mode with the addition of a trained model that can either ...

1) User: Manual control of both steering and throttle

2) Local Angle: Automatically control the steering angle

3) Local pilot: Automatically control both the steering angle and throttle amount

The web browser provides a drop-down menu to select between these options.

It is recommended to just start with Local Angle ... and control the throttle manually with the "i" key (faster) and "k" key (slower).

Neopixels
---------

On the car, do this::

    pip install rpi_ws281x adafruit-circuitpython-neopixel

The details come from here: https://learn.adafruit.com/neopixels-on-raspberry-pi/overview

::

    (env) pi@ohmc_24:~/ohmc_car$ diff -u manage.py.orig manage.py
    --- manage.py.orig	2018-06-11 08:14:34.079999106 +0000
    +++ manage.py	2018-06-11 08:37:05.959999560 +0000
    @@ -122,6 +122,9 @@
         tub = TubWriter(path=cfg.TUB_PATH, inputs=inputs, types=types)
         V.add(tub, inputs=inputs, run_condition='recording')
     
    +    pixels[0] = (32, 64, 128)
    +    pixels[1] = (0, 255, 0)
    +
         # run the vehicle
         V.start(rate_hz=cfg.DRIVE_LOOP_HZ,
                 max_loop_count=cfg.MAX_LOOPS)
    @@ -169,6 +172,13 @@
         cfg = dk.load_config()
     
         if args['drive']:
    +        # Neopixels
    +        # https://learn.adafruit.com/neopixels-on-raspberry-pi/python-usage
    +        import board
    +        import neopixel
    +        pixels = neopixel.NeoPixel(board.D18, 2)
    +        pixels[0] = (0, 0, 0)
    +        pixels[1] = (0, 64, 0)
             drive(cfg, model_path=args['--model'], use_chaos=args['--chaos'])

Then, you need to run as root for neopixels to work::

    sudo /home/pi/env/bin/python manage.py drive --model=/home/pi/ohmc_car/models/large_track.hdf5

Auto Start
----------

Add the following to ``/etc/rc.local`` to auto-start donkeycar::

    su - pi -c 'source ~/env/bin/activate && cd ~/ohmc_car; python manage.py drive &>/tmp/out' &


Add car to wifi network
-----------------------

Start ``wpa_cli`` and::

    > scan
    OK
    <3>CTRL-EVENT-SCAN-RESULTS
    > scan_results
    bssid / frequency / signal level / flags / ssid
    00:00:00:00:00:00 2462 -49 [WPA2-PSK-CCMP][ESS] MYSSID
    11:11:11:11:11:11 2437 -64 [WPA2-PSK-CCMP][ESS] ANOTHERSSID
    > add_network
    0
    > set_network 0 ssid "MYSSID"
    > set_network 0 psk "passphrase"
    > enable_network 0
    <2>CTRL-EVENT-CONNECTED - Connection to 00:00:00:00:00:00 completed (reauth) [id=0 id_str=]
    > save_config
    OK
